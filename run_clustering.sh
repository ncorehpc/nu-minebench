#!/bin/bash

export DMHOME=`pwd`

################## CLUSTERING ######################################
#---------------------------
# KMeans:Parallel
echo "------------- KMeans:Parallel ----------------"
#---------------------------
# Other datasets: color, texture
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/KMeans/example -i $DMHOME/datasets/kmeans/edge -b -o -p 8

#---------------------------
#Fuzzy Kmyypeans:Parallel
echo "--------------- Fuzzy Kmeans:Parallel -----------------"
#---------------------------
# Other datasets: color, texture
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/KMeans/example -i $DMHOME/datasets/kmeans/edge -b -o -f -p 8

#---------------------------
# birch:Serial
echo "--------------- birch:Serial ------------------"
#---------------------------
$DMHOME/BIRCH/birch $DMHOME/datasets/birch/AMR_64.para $DMHOME/datasets/birch/AMR_64.scheme $DMHOME/datasets/birch/AMR_64.proj $DMHOME/datasets/birch/particles_0_64_ascii

#---------------------------
echo "-------------------- HOP:Parallel --------------------"
#---------------------------
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/HOP/para_hop 61440 $DMHOME/datasets/HOP/particles_0_64 64 16 -1 8

