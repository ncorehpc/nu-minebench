#!/bin/bash

echo `date`

export DMHOME=`pwd`

###################### CLASSIFICATION #############################

#---------------------------
echo "--------------- ScalParC: OpenMP -----------------"
#---------------------------
env OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/scalparc $DMHOME/datasets/ScalParC/para_F26-A32-D250K/F26-A32-D250K.tab 250000 32 2 8

###################### ASSOCIATION RULE DISCOVERY #################

#---------------------------
echo "----------------- APRIORI:Parallel -----------------"
#---------------------------
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/omp_apriori -i $DMHOME/datasets/APR/data.ntrans_1000.tlen_10.nitems_1.npats_2000.patlen_6 -f $DMHOME/datasets/APR/offset_file_1000_10_1_P8.txt -s 0.0075 -n 8

#---------------------------
echo "----------------   ECLAT:Serial ---------------------"
#---------------------------
$DMHOME/eclat -i $DMHOME/datasets/ECLAT/ntrans_2000.tlen_20.nitems_1.npats_2000.patlen_6 -e 30 -s 0.0075

####################### PATTERN RECOGNITION #########################

#---------------------------
echo "--------------------- RSEARCH:Parallel(openmp) ------------------------"
#---------------------------
#NOTE: Set OMP_NUM_THREADS in console environment before issuing this command
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close  $DMHOME/rsearch -n 1000 -c -E 10 -m $DMHOME/datasets/rsearch/matrices/RIBOSUM85-60.mat \
                                                                                                  $DMHOME/datasets/rsearch/Queries/mir-40.stk \
                                                                                                  $DMHOME/datasets/rsearch/Databasefile/100Kdb.fa
##################### SUPPORT VECTOR MACHINES #########################

#---------------------------
echo "---------------------- SVM_RFE:Parallel ------------------------"
#---------------------------
#NOTE: Set OMP_NUM_THREADS in console environment before issuing this command
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/svm $DMHOME/datasets/SVM-RFE/outData.txt 253 15154 30

###################### DYNAMIC PROGRAMMING ############################

#---------------------------
echo "---------------------- PLSA:Parallel --------------------------"
#---------------------------
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/parasw.mt $DMHOME/datasets/PLSA/30k_1.txt $DMHOME/datasets/PLSA/30k_2.txt $DMHOME/datasets/PLSA/pam120.bla 600 400 3 3 1 8

echo `date`
