#!/bin/bash

echo `date`

export DMHOME=`pwd`

################## CLUSTERING ######################################

#---------------------------
echo "----------- KMeans:Parallel ---------------"
#---------------------------
# Other datasets: color, texture
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/KMeans/example -i $DMHOME/datasets/kmeans/edge -b -o -p 8

#---------------------------
echo "--------------- Fuzzy Kmeans:Parallel -------------------"
#---------------------------
# Other datasets: color, texture
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/KMeans/example -i $DMHOME/datasets/kmeans/edge -b -o -f -p 8

#---------------------------
echo "--------------- birch:Serial ----------------------------"
#---------------------------
$DMHOME/BIRCH/birch $DMHOME/datasets/birch/AMR_64.para $DMHOME/datasets/birch/AMR_64.scheme $DMHOME/datasets/birch/AMR_64.proj $DMHOME/datasets/birch/particles_0_64_ascii

#---------------------------
echo "------------- HOP:Parallel ---------------"
#---------------------------
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/HOP/para_hop 61440 $DMHOME/datasets/HOP/particles_0_64 64 16 -1 8

#Other dataset combinations:
#* 491520, _0_128
#* 3932160, _0_256

###################### CLASSIFICATION #############################

#---------------------------
echo "--------------- BAYESIAN:Serial -----------------"
#---------------------------
$DMHOME/Bayesian/bayes/src/bci -d $DMHOME/datasets/Bayesian/F26-A64-D250K_bayes.dom $DMHOME/datasets/Bayesian/F26-A64-D250K_bayes.tab $DMHOME/datasets/Bayesian/F26-A64-D250K_bayes.nbc

#---------------------------
echo "--------------- ScalParC: OpenMP -----------------"
#---------------------------
env OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/ScalParC/scalparc $DMHOME/datasets/ScalParC/para_F26-A32-D250K/F26-A32-D250K.tab 250000 32 2 8

###################### ASSOCIATION RULE DISCOVERY #################

#---------------------------
echo "----------------- APRIORI:Parallel -----------------"
#---------------------------
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/Apriori/omp_apriori -i $DMHOME/datasets/APR/data.ntrans_1000.tlen_10.nitems_1.npats_2000.patlen_6 -f $DMHOME/datasets/APR/offset_file_1000_10_1_P8.txt -s 0.0075 -n 8

#There is a utility file, offsets.c, that can generate offsets for different
#number of threads. For example, use the following commands to generate a list
#of offsets for 4 threads.
#make offsets
#offsets datafile 4
#(example datafile can be NU-MineBench/datasets/2.0.data1/datasets/APR/data.ntrans_8000.tlen_20.nitems_1.npats_2000.patlen_6)

#---------------------------
echo "----------------   ECLAT:Serial ---------------------"
#---------------------------
$DMHOME/ECLAT/eclat -i $DMHOME/datasets/ECLAT/ntrans_2000.tlen_20.nitems_1.npats_2000.patlen_6 -e 30 -s 0.0075

#----------------------------------------------
# Utility_Mining:Parallel with real dataset
#----------------------------------------------
# env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/Utility_Mining/para_tran_utility/utility_mine $DMHOME/datasets/utility_mine/RealData/real_data_aa_binary $DMHOME/datasets/utility_mine/RealData/real_data_aa_binary_P8.txt $DMHOME/datasets/utility_mine/RealData/product_price_binary 0.01 8

#----------------------------------------------
# Utility_Mining:Parallel with synthetic dataset
#----------------------------------------------
# env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/Utility_Mining/para_tran_utility/utility_mine $DMHOME/datasets/utility_mine/GEN/data.ntrans_1000.tlen_10.nitems_1.patlen_6 $DMHOME/datasets/utility_mine/GEN/offset_1000_10_1/offset_1000_10_1_6_P8.txt $DMHOME/datasets/utility_mine/GEN/logn1000_binary 0.01 8

#----------------------------------------------
#echo "-------------------- Utility_Mining:Serial with real dataset ----------------------------"
#----------------------------------------------
#$DMHOME/Utility_Mining/tran_utility/utility_mine $DMHOME/datasets/utility_mine/RealData/real_data_aa_binary  $DMHOME/datasets/utility_mine/RealData/product_price_binary 0.02

#----------------------------------------------
#echo "---------------------- Utility_Mining:Serial with synthetic dataset -----------------------------"
#----------------------------------------------
#$DMHOME/Utility_Mining/tran_utility/utility_mine $DMHOME/datasets/utility_mine/GEN/data.ntrans_1000.tlen_10.nitems_1.patlen_6 $DMHOME/datasets/utility_mine/GEN/logn1000_binary 0.02

########################## BAYESIAN NETWORK #########################

#---------------------------
#SNP:Parallel
#---------------------------
#NOTE: Set OMP_NUM_THREADS (using the export, set, or setenv command) in console environment before issuing this command
# env OMP_NUM_THREADS=8 $DMHOME/SNP/pnl.snps/snp/snp  $DMHOME/datasets/SNP/snp_s_ar

#---------------------------
#GeneNet:Parallel  NOTE: **** This is not in the distribution ****
#---------------------------
#NOTE: Set OMP_NUM_THREADS in console environment before issuing this command
# env OMP_NUM_THREADS=8 $DMHOME/GeneNet/pnl.genenet/genenet/genenet.icc $DMHOME/datasets/GeneNet/microarray.int

######################## EXPECTATION MAXIMIZATION ###################

#---------------------------
#semphy:Serial
#---------------------------
#  original command line:  $DMHOME/semphy/bin/semphy -s $DMHOME/datasets/semphy/108.phy -f phylip -m jtt -G 0.3
# $DMHOME/SEMPHY/bin/semphy -s $DMHOME/datasets/semphy/108.phy --jtt -A 0.3 --posteriorDTME -O

#---------------------------
#semphy:Parallel - NOTE: **** there is no parallel version in this distribution ****
#---------------------------
#NOTE: Set OMP_NUM_THREADS in console environment before issuing this command
# env OMP_NUM_THREADS=8 $DMHOME/semphy/semphy.mt -s $DMHOME/datasets/semphy/108.phy --posteriorDTME --jtt -A 0.3 -O

####################### PATTERN RECOGNITION #########################

#---------------------------
echo "--------------------- RSEARCH:Parallel(openmp) ------------------------"
#---------------------------
#NOTE: Set OMP_NUM_THREADS in console environment before issuing this command
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close  $DMHOME/RSEARCH/rsearch -n 1000 -c -E 10 -m $DMHOME/datasets/rsearch/matrices/RIBOSUM85-60.mat \
                                                                                                  $DMHOME/datasets/rsearch/Queries/mir-40.stk \
                                                                                                  $DMHOME/datasets/rsearch/Databasefile/100Kdb.fa

##################### SUPPORT VECTOR MACHINES #########################

#---------------------------
echo "---------------------- SVM_RFE:Parallel ------------------------"
#---------------------------
#NOTE: Set OMP_NUM_THREADS in console environment before issuing this command
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/SVM-RFE/svm $DMHOME/datasets/SVM-RFE/outData.txt 253 15154 30

###################### DYNAMIC PROGRAMMING ############################

#---------------------------
echo "---------------------- PLSA:Parallel --------------------------"
#---------------------------
env OMP_NUM_THREADS=8 OMP_PLACES=cores OMP_PROC_BIND=close $DMHOME/PLSA/parasw $DMHOME/datasets/PLSA/30k_1.txt $DMHOME/datasets/PLSA/30k_2.txt $DMHOME/datasets/PLSA/pam120.bla 600 400 3 3 1 8

###################### Rules ##########################################

#---------------------------
#AFI:Serial
#---------------------------
#$DMHOME/AFI/afi $DMHOME/datasets/ETI/<dataset> minsup epsCol epsRow

#---------------------------
#Recursive Weak:Serial
#---------------------------
#$DMHOME/Recursive_Weak/rw $DMHOME/datasets/ETI/<dataset> minsup eps

#---------------------------
#Recursive Weak with post processing:Serial
#---------------------------
#$DMHOME/Recursive_Weak_pp/rwpp $DMHOME/datasets/ETI/<dataset> minsup epsCol epsRow

#---------------------------
#Parallel ETI:Parallel(MPI)
#---------------------------
#$DMHOME/ParETI/pareti $DMHOME/datasets/ETI/<dataset> support epsilon hconf
#where N is the number of processors

echo `date`

