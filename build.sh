#!/bin/bash

export DMHOME=`pwd`

# ECLAT:
cd $DMHOME/ECLAT
make
cd $DMHOME

# Bayesian:
cd $DMHOME/Bayesian/bayes/src
make
cd $DMHOME

# ScalParC:
cd $DMHOME/ScalParC/
make
cd $DMHOME

# birch:
#cd $DMHOME/BIRCH
#make
#cd $DMHOME

# kmeans:
cd $DMHOME/KMeans
make example
cd $DMHOME

# hop:
cd $DMHOME/HOP
make
cd $DMHOME

# SNP:
#cd $DMHOME/SNP/pnl.snps/pnl/c_pgmtk/src
#make
#cd $DMHOME

#cd $DMHOME/SNP/pnl.snps/snp
#make
#cd $DMHOME

# semphy:
cd $DMHOME/SEMPHY
make -f Makefile;make install
cd $DMHOME

# rsearch:
#Offered in two parallel flavors, based on OpenMP and MPI.
# For OpenMP version of rsearch,
cd $DMHOME/RSEARCH
./configure CXX=clang++-16 CC=clang-16 CFLAGS="-O3 -march=x86-64-v3 -ffast-math -Rpass=loop-vectorize -fopenmp" LIBS=-lm
make
cd $DMHOME

#PLSA:
cd $DMHOME/PLSA
make -f Makefile.omp
cd $DMHOME

#SVM-RFE:
cd $DMHOME/SVM-RFE/
make
cd $DMHOME

#utility_mine:
cd $DMHOME/Utility_Mining/para_tran_utility
make
cd $DMHOME

# AFI:
cd $DMHOME/AFI
make
cd $DMHOME

# rw:
cd $DMHOME/Recursive_Weak
make
cd $DMHOME

# rwpp:
cd $DMHOME/Recursive_Weak_pp
make
cd $DMHOME

# ParETI:
cd $DMHOME/ParETI
make
cd $DMHOME
