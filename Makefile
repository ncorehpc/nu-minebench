SUBDIRS = KMeans BIRCH HOP ScalParC SEMPHY ECLAT RSEARCH SVM-RFE PLSA AFI Recursive_Weak Recursive_Weak_pp ParETI Apriori
#SUBDIRS = KMeans BIRCH HOP Bayesian ScalParC SEMPHY ECLAT RSEARCH SVM-RFE PLSA AFI Recursive_Weak Recursive_Weak_pp ParETI Apriori

all:
	echo $(.INCLUDE_DIRS)
	@set -e ; for i in $(SUBDIRS) ; do ( cd $$i && make ARCH=$(ARCH) all ; ) ; done

clean: FORCE
	@set -e ; for i in $(SUBDIRS) ; do ( cd $$i && make clean ; ) ; done

.PHONY: FORCE all clean
